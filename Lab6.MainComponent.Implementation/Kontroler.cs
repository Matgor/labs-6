﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.MainComponent.Contract;
using Lab6.Display.Contract;


namespace Lab6.MainComponent.Implementation
{
    public class Kontroler : IKontroler
    {
        private IDisplay wyswietlacz;

        public Kontroler(IDisplay wysw)
        {
            this.wyswietlacz = wysw;
        }

        public void Metoda1()
        {
            Console.WriteLine("Test1");
        }

        public void Metoda2()
        {
            Console.WriteLine("Test2");
        }

    }
}
