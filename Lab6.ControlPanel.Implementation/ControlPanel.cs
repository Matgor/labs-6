﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using Lab6.MainComponent.Implementation;
using Lab6.ControlPanel.Implementation;
namespace Lab6.ControlPanel.Implementation
{
    public class ControlPanel : IControlPanel
    {
        private PanelKontrolny panelkontrol = new PanelKontrolny();

        private IKontroler kontrol;
        public ControlPanel(IKontroler kontrol)
        {
            this.kontrol = kontrol;
        }

        public System.Windows.Window Window
        {
            get { return panelkontrol; }
        }
    }
}
